#pragma once

namespace libbackup_distributed
{

struct exception : public std::exception
{
    /* Convenience for concatenating several strings into one for a reason. */
    template <typename... As>
    exception(As...);
    char const * what(void) const noexcept;
    std::string const reason;
};

inline std::string concat(std::string const & str)
{
    return str;
}

template <typename... As>
std::string concat(std::string const & lhs, std::string const & rhs, As... as)
{
    return concat(lhs + rhs, as...);
}

template <typename... As>
exception::exception(As... as)
    : reason {concat(as...)}
{
}

} /* namespace libbackup_distributed */
