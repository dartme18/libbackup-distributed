DEBUG=true
CC=g++
ifeq "$(DEBUG)" "true"
DEBUGOPTS=-g -O0
else
DEBUGOPTS=-O3 -DNDEBUG
endif
CXXFLAGS=-Wall -Wextra -pedantic -Werror -std=c++2a $(DEBUGOPTS) -fpic -fdiagnostics-color=always
sources=$(wildcard src/*.cpp)
objects=$(sources:src/%.cpp=obj/%.o)
headers=$(wildcard src/*.hpp)
headers+=$(wildcard include/libbackup-distributed/*.hpp)
testsources=$(wildcard src/test/*.cpp)
testobjects=$(testsources:src/%.cpp=obj/%.o)
tests=$(filter-out dist/test/logging,$(testobjects:obj/test/%.o=dist/test/%))
testruntargets=$(tests:dist/%=%)
precompiledheader=src/pch.hpp.gch
output=dist/libbackup-distributed.so
testresourcesrc=$(shell find testresources/ -type f)
testresourcedest=$(testresourcesrc:testresources/%=dist/%)
headerdependencies=$(objects:obj/%.o=obj/%.d) $(testobjects:obj/%.o=obj/%.d)

# "|" signals an order-only prerequisite: the prereq must exist, but
# it won't cause the target to be recreated.
$(output): $(precompiledheader) $(objects) Makefile | obj dist
	echo Making $@
	$(CC) $(CXXFLAGS) -shared -o $@ \
	$(objects)
	echo "Made  " $@

obj dist obj/test dist/test :
	if [ ! -e $@ ]; then mkdir -p $@; fi;

-include $(headerdependencies)

reformat:
	for file in $(sources) $(testsources) $(headers); do \
		clang-format -i -style="$$(<style)" "$$file"; \
	done;

# Static pattern rules. Take each object in the targets, create a "base"
# with it specified by % (removing the literal part '.o'), then
# use that base to generate the prerequisite. This generates
# several rules.  For instance,  main.o : "main".o : "main".cpp
# then refer to the left-most prerequisite with $<, and the target
# with $@ like usual.
$(objects) : obj/%.o : src/%.cpp $(precompiledheader) Makefile | obj
	@echo Making $@
	@$(CC) -c $(CXXFLAGS) -MMD \
		-D LOG4CPPCONFFILE=\"log4cpp.properties\" \
		-Iinclude \
		$< -o $@
	@echo "Made  " $@

$(precompiledheader) : src/pch.hpp Makefile | obj
	@echo Making $@
	@$(CC) $(CXXFLAGS) \
	-D LOG4CPPCONFFILE=\"log4cpp.properties\" \
		$< -o $@
	@echo "Made  " $@

test : CXXFLAGS+=--coverage
test : $(tests) dist/run_tests

dist/run_tests : Makefile dist/run_tests_template | dist
	@cd dist && \
		cp run_tests_template run_tests && \
		sed -i "s/%%testrun%%/$(subst /,\/,$(testruntargets))/g" run_tests && \
		sed -i "s/%%sources%%/$(subst /,\/,$(sources))/g" run_tests && \
		chmod +x run_tests;

short-check : test dist/run_tests Makefile | dist
	@cd dist; ./run_tests

check : $(testruntargets) test

$(testruntargets) : % : dist/% Makefile | dist
	@cd dist; valgrind --leak-check=full ./$@

$(testresourcedest) : dist/% : testresources/% | dist
	@mkdir -p $(dir $@);
	@cp -r $< $@;

$(tests) : dist/test/% : obj/test/%.o $(precompiledheader) $(objects) Makefile $(objects) | dist $(testresourcedest) dist/test
	@echo Making $@
	@$(CC) $(CXXFLAGS) -o $@ -pthread -llog4cpp -lgtest \
		$< $(objects)
	@echo "Made  " $@

$(testobjects) : obj/test/%.o : src/test/%.cpp Makefile $(precompiledheader) | obj/test
	@echo Making $@
	@$(CC) -c $(CXXFLAGS) -MMD -D LOG4CPPCONFFILE=\"log4cpp.properties\" \
		-Isrc -Iinclude \
		$< -o $@
	@echo "Made  " $@

clean :
	rm -rf obj dist $(precompiledheader)

echo :
	@echo sources $(sources)
	@echo objects $(objects)
	@echo headers $(headers) src/pch.hpp
	@echo executable $(executable)
	@echo testresourcesrc $(testresourcesrc)
	@echo testresourcedest $(testresourcedest)
	@echo tests $(tests)
	@echo testsources $(testsources)
	@echo testobjects $(testobjects)
	@echo testruntargets $(testruntargets)
	@echo headerdependencies $(headerdependencies)

.PHONY : echo clean reformat test check $(testruntargets)
