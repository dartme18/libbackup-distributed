#pragma once

#include <log4cpp/Category.hh>

extern unsigned thread_local indent_level;

log4cpp::Category & logimpl(std::string const &);
std::string get_category_name(char const *);

#define GETLOGGER() logimpl(get_category_name(__FILE__).c_str())

template <class... A>
void error_func(std::string const & logger, unsigned linenum, char const * message, A &&... a)
{
    std::string m {"ERROR " + logger + ":%03d -- "};
    m += message;
    logimpl(logger).error(m.c_str(), linenum, a...);
}

template <class... A>
void warn_func(std::string const & logger, unsigned linenum, char const * message, A &&... a)
{
    std::string m {"WARN  " + logger + ":%03d -- "};
    m += message;
    logimpl(logger).warn(m.c_str(), linenum, a...);
}

template <class... A>
void debug_func(std::string const & logger, unsigned linenum, char const * message, A &&... a)
{
    std::string indentation(indent_level * 2, ' ');
    std::string m {"debug " + indentation + logger + ":%03d -- "};
    m += message;
    logimpl(logger).debug(m.c_str(), linenum, a...);
}

template <class... A>
void debug_func(bool indent, std::string const & logger, unsigned linenum, char const * message, A &&... a)
{
    if (!indent && indent_level)
        --indent_level;
    debug_func(logger, linenum, message, a...);
    if (indent)
        ++indent_level;
}

template <class... A>
void info_func(std::string const & logger, unsigned linenum, char const * message, A &&... a)
{
    std::string m {"info  " + logger + ":%03d -- "};
    m += message;
    logimpl(logger).info(m.c_str(), linenum, a...);
}
#define ERROR(...) error_func(get_category_name(__FILE__), __LINE__, __VA_ARGS__)
#define DEBUG(...) debug_func(get_category_name(__FILE__), __LINE__, __VA_ARGS__)
#define DEBUG_IN(...) debug_func(true, get_category_name(__FILE__), __LINE__, __VA_ARGS__)
#define DEBUG_OUT(...) debug_func(false, get_category_name(__FILE__), __LINE__, __VA_ARGS__)
#define WARN(...) warn_func(get_category_name(__FILE__), __LINE__, __VA_ARGS__)
#define INFO(...) info_func(get_category_name(__FILE__), __LINE__, __VA_ARGS__)
