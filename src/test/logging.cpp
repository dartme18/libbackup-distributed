#include "pch.hpp"

#include "logging.hpp"
#include <log4cpp/Appender.hh>
#include <log4cpp/PropertyConfigurator.hh>

unsigned thread_local indent_level {0};

struct logging_initer
{
    logging_initer(void)
    {
        std::cout << "Initializing logging with properties file, \"" << LOG4CPPCONFFILE << "\"\n";
        log4cpp::PropertyConfigurator::configure(LOG4CPPCONFFILE);
    }
};

log4cpp::Category & logimpl(std::string const & logname)
{
    /* Configure logging once and thread-safe */
    static logging_initer l;
    return log4cpp::Category::getInstance(logname.c_str());
}

std::string get_category_name(char const * file)
{
    std::string name {file};
    auto index = name.find_last_of('/');
    name = name.substr(index + 1);
    index = name.find_last_of('.');
    name = name.substr(0, index);
    //std::cout << "Creating category " << file << " (" << name << ")\n";
    return name;
}
