#include "pch.hpp"

#include <libbackup-distributed/backup-distributed.hpp>

namespace libbackup_distributed
{

void say_hello(void)
{
    std::cout << "Hello, world.\n";
}

} /* namespace libbackup_distributed */
